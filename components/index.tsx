import {AppBar, Box, Button, Container, Toolbar, Typography} from "@mui/material";
import SmartToyIcon from '@mui/icons-material/SmartToy';
import {FCC} from "../utils";


export const MobileMenu: FCC = () => {
    return <Box className="md:hidden flex mr-1 flex-grow items-center" />;
};

export const DesktopMenu: FCC = () => {
    return <Box className="mr-1 flex items-center">
        <SmartToyIcon className="mr-1" />
        <Typography
            variant="h6"
            noWrap
            component="a"
            href="/"
            sx={{
                mr: 2,
                fontFamily: 'monospace',
                fontWeight: 700,
                letterSpacing: '.3rem',
                color: 'inherit',
                textDecoration: 'none',
            }}
        >
            Blogjob
        </Typography>
        <Box>
            <Button className="text-white">abba</Button>
        </Box>
    </Box>;
};

export const Header: FCC = ({children}) => {
    return (
        <AppBar position="static">
            <Container maxWidth="lg">
                <Toolbar disableGutters>
                    <DesktopMenu />
                    {/*<MobileMenu /> */}
                </Toolbar>
            </Container>
        </AppBar>
    );
};