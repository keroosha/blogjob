import { ThemeOptions } from "@mui/material";

const defaultTheme: ThemeOptions = {
  palette: {
    mode: "dark",
  },
};

export default defaultTheme;
