import createCache from "@emotion/cache";
import { FC, ReactNode } from "react";

export const createEmotionCache = () => createCache({ key: "css", prepend: true });

export type FCC<T extends {} = {}> = FC<{ children?: ReactNode } & T>;
