import "../styles/globals.css";
import { createEmotionCache } from "../utils";
import DefaultTheme from "../styles/theme/default-theme";
import { CacheProvider } from "@emotion/react";
import { createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import { Header } from "../components";

const eCache = createEmotionCache();
const theme = createTheme(DefaultTheme);

const MyApp = ({ Component, emotionCache = eCache, pageProps }) => (
  <CacheProvider value={emotionCache}>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header></Header>
      <Component {...pageProps} />
    </ThemeProvider>
  </CacheProvider>
);

export default MyApp;
